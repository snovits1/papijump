Stick Jump by Steven Novitske

Jump from block to block as high as you can. Fall below the last block on the screen and you lose.

Gray blocks bounce you at normal speed.
Orange blocks bounce you up at speed 2x that of gray blocks.
Green blocks bounce you up at speed 2x that of orange blocks.
Red blocks serve no purpose other than to distract you.
Yellow blocks give you a life saver for each time you land on one, which allows you to bounce back up from the bottom. You can only have a max of 5 life savers at once. These blocks bounce the same as gray ones.

Designed specifically for iPhone 6S.