//
//  ViewController.h
//  p03-novitske
//
//  Created by Steven Novitske on 2/16/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSTimer *timer;
    __weak IBOutlet UIImageView *jumper;
    __weak IBOutlet UISlider *xSpeed;
}
-(void)arrange;
@end

