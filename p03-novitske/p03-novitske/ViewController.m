//
//  ViewController.m
//  p03-novitske
//
//  Created by Steven Novitske on 2/16/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *gameOver2;
@property (weak, nonatomic) IBOutlet UILabel *gameOver;
@property (weak, nonatomic) IBOutlet UIButton *resartLabel;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UILabel *score;
@property (weak, nonatomic) IBOutlet UILabel *lives;
@end

NSMutableArray *bricks;
CGPoint startPoint;
int scoreVal;
float dy;
float dx;
int numBricks;
float brickWidth;
float brickHeight;
float xMax;
float yMax;
float jumperBottom;
bool paused;
int lifeSavers; //Power up that bounces them up if fallen too far

@implementation ViewController

//Stop the timer
- (IBAction)pauseGame:(id)sender {
    if([_pauseButton.titleLabel.text isEqualToString:@"Pause"]) {
        [_pauseButton setTitle:@"Resume" forState:(UIControlStateNormal)];
        paused = true;
    } else {
        [_pauseButton setTitle:@"Pause" forState:(UIControlStateNormal)];
        paused = false;
    }
}

- (IBAction)restartGame:(id)sender {
//Clear and redraw bricks
    for(int i=0; i<numBricks; i++) {
        UIView *b = bricks[i];
        [b removeFromSuperview];
    }
    [bricks removeAllObjects];
    [self initializeBricks];
//Initialize everything else
    _score.text = @"Score: 0";
    _gameOver.text = @"";
    _gameOver2.text = @"";
    scoreVal = 0;
    xSpeed.value = 0;
    numBricks = 12;
    jumperBottom = jumper.center.y + jumper.bounds.size.height/2;
    dy = 30;
    jumper.center = startPoint;
    paused = false;
    [_pauseButton setTitle:@"Pause" forState:UIControlStateNormal];
    lifeSavers = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed: 180.0/255.0 green: 238.0/255.0 blue:250.0/255.0 alpha: 1];
//Set timer to call arrange 30 times per second (0.033)
    timer = [NSTimer scheduledTimerWithTimeInterval:0.033 target:self selector:@selector(arrange) userInfo:nil repeats:YES];
//Initalize jumper settings
    jumper.image = [UIImage imageNamed: @"stickguyright.png"];
    jumperBottom = jumper.center.y + jumper.bounds.size.height/2;
    startPoint = jumper.center;
    dy = 30;
//Get bounds of the playing screen for brick placements
    xMax = [UIScreen mainScreen].bounds.size.width;
    yMax = [UIScreen mainScreen].bounds.size.height-80;
    brickWidth = xMax*.17;
    brickHeight = 10;
    numBricks = 12;
    [self initializeBricks];
    paused = false;
    scoreVal = 0;
    lifeSavers = 0;
}


//Main function
-(void)arrange {
    if(paused == false) {
        dy -= 2;
        dx = xSpeed.value;
        //Change stick man direction when needed
        if(dx >= 0) jumper.image = [UIImage imageNamed: @"stickguyright.png"];
        else jumper.image = [UIImage imageNamed: @"stickguyleft.png"];
        //Wrap around screen if out of bounds
        if(jumper.center.x > xMax) jumper.center = CGPointMake(0, jumper.center.y);
        else if(jumper.center.x < 0) jumper.center = CGPointMake(xMax, jumper.center.y);
        //Check if jumper lands on platform
        jumperBottom = jumper.center.y + jumper.bounds.size.height/2;
        if(dy <= 0) {
            [self bounce];
        }
        //Move jumper according to slider value and current y speed
        if(jumperBottom >= yMax*0.5 || dy < 0) {
            jumper.center = CGPointMake(jumper.center.x + dx, jumper.center.y - dy);
            //Move bricks down if jumper reaches certain height
        } else if(jumperBottom < yMax*0.5) {
            jumper.center = CGPointMake(jumper.center.x + dx, jumper.center.y);
            [self moveBricks];
        }
        [self removeBrick];
        [self checkGameOver];
    }
}

//Moves bricks down and back up top, creating powerup bricks at random times
- (void)moveBricks {
    for(int i=0; i<numBricks; i++) {
        UIView *brick = bricks[i];
        UIView *prev;
        if(i > 0) prev = bricks[i-1];
        else prev = bricks[numBricks-1];
        brick.center = CGPointMake(brick.center.x, brick.center.y + dy);
        if(brick.center.y > yMax) {
            int chance = rand() % 100;
            if(chance < 3) { // 3% chance of spring brick
                [brick setBackgroundColor:[UIColor orangeColor]];
            } else if(chance == 99) { // 1% chance of super spring brick
                [brick setBackgroundColor:[UIColor greenColor]];
            } else if(chance >=3 && chance < 8 && numBricks > 4) { // 6% chance of unusable brick
                [brick setBackgroundColor:[UIColor redColor]];
            } else if(chance == 98 && numBricks > 5) { // 1% chance of life saver brick
                [brick setBackgroundColor:[UIColor yellowColor]];
            } else {
                [brick setBackgroundColor:[UIColor grayColor]];
            }
            brick.center = CGPointMake(brickWidth/2 + rand() % (int)(xMax-brickWidth), prev.center.y - yMax/numBricks+5);
        }
    }
    scoreVal += dy;
    _score.text = [NSString stringWithFormat:@"Score: %i",scoreVal];
}

//Returns index of brick closest to bottom
- (int)topBrick {
    int maxY = 1000;
    int index = 0;
    for(int i=0; i<numBricks; i++) {
        UIView *brick = bricks[i];
        if(brick.center.y < maxY) {
            maxY = brick.center.y;
            index = i;
        }
    }
    return index;
}

//Checks if jumper falls below screen
- (void)checkGameOver {
    if(jumper.center.y > yMax + 100) {
        if(lifeSavers > 0) {
            dy = 45;
            lifeSavers--;
            _lives.text = [NSString stringWithFormat:@"Life Savers: %i",lifeSavers];
        } else {
            [self.view bringSubviewToFront:(_gameOver2)];
            _gameOver2.text = @"Game Over";
            [self.view bringSubviewToFront:(_gameOver)];
            _gameOver.text = @"Game Over";
        }
    }
}

//Removes a brick for every 2000 points down to 5 bricks
- (void)removeBrick {
    if((numBricks == 5 && scoreVal > 50000) ||
       (numBricks == 6 && scoreVal > 27000) ||
       (numBricks == 7 && scoreVal > 20000) ||
       (numBricks == 8 && scoreVal > 14000) ||
       (numBricks == 9 && scoreVal > 9000)  ||
       (numBricks == 10 && scoreVal > 5000) ||
       (numBricks == 11 && scoreVal > 2000) ||
       (numBricks == 12 && scoreVal > 1000)) {
        int top = [self topBrick];
        [bricks[top] removeFromSuperview];
        [bricks removeObjectAtIndex:top];
        numBricks--;
    }
}

//Bounces the stick guy if he is on a platform (or really close)
- (void)bounce {
    for(int i=0; i<numBricks; i++) {
        UIView *brick = bricks[i];
        if(jumper.center.x > brick.center.x - brickWidth/2 - jumper.bounds.size.width/2 &&
           jumper.center.x < brick.center.x + brickWidth/2 + jumper.bounds.size.width/2 &&
           jumperBottom > brick.center.y - 10 &&
           jumperBottom < brick.center.y + 20) {
            if(brick.backgroundColor == [UIColor orangeColor]) dy = 60;
            else if(brick.backgroundColor == [UIColor greenColor]) dy = 120;
            else if(brick.backgroundColor == [UIColor redColor]) {}
            else if(brick.backgroundColor == [UIColor yellowColor] && lifeSavers < 5) {
                lifeSavers++;
                _lives.text = [NSString stringWithFormat:@"Life Savers: %i",lifeSavers];
                dy = 30;
            } else dy = 30;
        }
    }
}

//Initializes bricks array and sets than at points with evenly spaced y values and random x values
- (void)initializeBricks {
    bricks = [[NSMutableArray alloc] init];
    for(int i=0; i<12; i++) {
        UIView *prev;
        int x, y;
        if(i>0) {
            prev = bricks[i-1];
            y = prev.center.y - yMax*.09;
            x = rand() % (int)(xMax-brickWidth);
        } else {
            x = xMax/2-brickWidth/2;
            y = yMax;
        }
        UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(x, y, brickWidth, brickHeight)];
        [brick setBackgroundColor:[UIColor grayColor]];
        [self.view addSubview:brick];
        [bricks addObject:brick];
        [self.view bringSubviewToFront:(_score)];
        [self.view bringSubviewToFront:(_pauseButton)];
        [self.view bringSubviewToFront:(_resartLabel)];
        [self.view bringSubviewToFront:(_lives)];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
